#!/usr/bin/env bash

docker stop build-openidc
docker rm build-openidc
docker build -t build-openidc .
docker run -dit --name build-openidc build-openidc
docker cp build-openidc:/usr/lib64/httpd/modules/mod_auth_openidc.so .